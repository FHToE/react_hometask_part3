package com.academy.homework.reactions.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class ReactionRequestDto {
    private UUID userId;
    private UUID messageId;
}
