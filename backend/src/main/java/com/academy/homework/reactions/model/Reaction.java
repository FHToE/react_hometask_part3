package com.academy.homework.reactions.model;

import com.academy.homework.db.BaseEntity;
import com.academy.homework.messages.model.Message;
import com.academy.homework.users.model.User;
import lombok.*;

import javax.persistence.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper=true)
@Entity
@Table(name = "reactions")
public class Reaction extends BaseEntity {

    @Column(name = "isLike")
    private Boolean isLike;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "message_id")
    private Message message;
}
