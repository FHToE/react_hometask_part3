package com.academy.homework.reactions;

import com.academy.homework.reactions.dto.ReactionResponseDto;
import com.academy.homework.reactions.dto.ReactionRequestDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/api/reactions")
public class ReactionController {
    @Autowired
    private ReactionService reactionService;

    @PostMapping
    private ReactionResponseDto setReaction(@RequestBody ReactionRequestDto request) {
        return reactionService.setReaction(request.getUserId(), request.getMessageId());
    }
}
