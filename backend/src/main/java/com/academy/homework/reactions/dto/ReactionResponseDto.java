package com.academy.homework.reactions.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

@Data
@AllArgsConstructor
public class ReactionResponseDto {
    private UUID id;
    private Boolean isLike;
}
