package com.academy.homework.reactions;

import com.academy.homework.reactions.dto.ReactionResponseDto;
import com.academy.homework.reactions.model.Reaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.academy.homework.users.UserRepository;
import com.academy.homework.messages.MessageRepository;

import java.util.UUID;

@Service
public class ReactionService {
    @Autowired
    private ReactionsRepository reactionsRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private MessageRepository messageRepository;

    public ReactionResponseDto setReaction(UUID userId, UUID messageId) {
        var reaction = reactionsRepository.findReaction(userId,messageId);
        ReactionResponseDto result;
        if (reaction.isEmpty()) {
            Reaction newReaction =Reaction.builder().isLike(true)
                .message(messageRepository.getMessageById(messageId).orElseThrow())
                .user(userRepository.findUserById(userId).orElseThrow())
                .build();
            reactionsRepository.save(newReaction);
            result = new ReactionResponseDto(newReaction.getId(), newReaction.getIsLike());
        } else {
            Reaction updated = reactionsRepository.getReactionById(reaction.get().getId()).orElseThrow();
            updated.setIsLike(!updated.getIsLike());
            reactionsRepository.save(updated);
            result = reaction.get();
            result.setIsLike(!result.getIsLike());
        }
        return result;
    }
}
