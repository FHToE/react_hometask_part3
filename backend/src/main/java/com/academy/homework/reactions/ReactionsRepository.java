package com.academy.homework.reactions;

import com.academy.homework.reactions.dto.ReactionResponseDto;
import com.academy.homework.reactions.model.Reaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ReactionsRepository extends JpaRepository<Reaction, UUID> {

    @Query("SELECT new com.academy.homework.reactions.dto.ReactionResponseDto(r.id, r.isLike) from Reaction r " +
            "WHERE r.message.id = :messageId AND r.user.id = :userId")
    Optional<ReactionResponseDto> findReaction(@Param("userId") UUID userId, @Param("messageId") UUID messageId);

    Optional<Reaction> getReactionById(UUID id);

    List<Reaction> getReactionsByUser_Id(UUID id);
}
