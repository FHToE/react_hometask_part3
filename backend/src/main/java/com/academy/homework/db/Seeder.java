package com.academy.homework.db;

import com.academy.homework.messages.MessageRepository;
import com.academy.homework.messages.model.Message;
import com.academy.homework.reactions.ReactionsRepository;
import com.academy.homework.reactions.model.Reaction;
import com.academy.homework.users.UserRepository;
import com.academy.homework.users.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;

@Component
public class Seeder {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private MessageRepository messageRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ReactionsRepository reactionsRepository;

    @EventListener
    public void seed(ContextRefreshedEvent event) {
        List<User> u = jdbcTemplate.query("SELECT * FROM users", (resultSet, rowNum) -> null);
        if(u == null || u.size() <= 0) {
            seed();
        }
    }

    private void seed() {
        User Ruth = User.builder().name("Ruth").
                avatar("https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1" +
                        ".cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA").password("123456").role("user").email("Ruth@mail.com").build();
        User Wendy = User.builder().name("Wendy").
                avatar("https://resizing.flixster.com/EVAkglctn7E9B0hVKJrueplabuQ=/220x196/v1.cj" +
                        "s0NjYwNjtqOzE4NDk1OzEyMDA7MjIwOzE5Ng").password("123456").role("user").email("Wendy@mail.com").build();
        User Helen = User.builder().name("Helen").
                avatar("https://resizing.flixster.com/PCEX63VBu7wVvdt9Eq-FrTI6d_4=/300x300/v1.cjs0Mz" +
                        "YxNjtqOzE4NDk1OzEyMDA7MzQ5OzMxMQ").password("123456").role("user").email("Helen@mail.com").build();
        User Ben = User.builder().name("Ben").
                avatar("https://www.aceshowbiz.com/images/photo/tom_pelphrey.jpg").password("123456").role("user").email("Ben@mail.com").build();
        User admin = User.builder().name("admin").
                avatar("https://pickaface.net/gallery/avatar/20130319_083314_1174_admin.png").password("admin").role("admin").email("admin@mail.com").build();
        List<User> users = Arrays.asList(Ruth, Wendy, Helen, Ben, admin);

        Message m1 = Message.builder().text("I don’t *** understand. It's the Panama accounts").user(Ruth).build();
        m1.setCreatedAt(new Date(120,6,16,19,48));
        Message m2 = Message.builder().text("Tells exactly what happened.").user(Wendy).build();
        m2.setCreatedAt(new Date(120,6,16,19,49));
        m2.setEditedAt(new Date(120,6,16,19,49));
        Message m3 = Message.builder().text("You were doing your daily bank transfers and…").user(Wendy).build();
        m3.setCreatedAt(new Date(120,6,16,19,47));
        Message m4 = Message.builder().text("Yes, like I’ve been doing every *** day without red *** flag").user(Ruth).build();
        m4.setCreatedAt(new Date(120,6,16,19,45));
        Message m5 = Message.builder().text("There`s never been a *** problem.").user(Ruth).build();
        m5.setCreatedAt(new Date(120,6,16,19,50));
        Message m6 = Message.builder().text("Why this account?").user(Helen).build();
        m6.setCreatedAt(new Date(120,6,17,15,45));
        Message m7 = Message.builder().text("I don`t *** know! I don`t know!").user(Ruth).build();
        m7.setCreatedAt(new Date(120,6,17,16,45));
        Message m8 = Message.builder().text("What the ** is a red flag anyway?").user(Ben).build();
        m8.setCreatedAt(new Date(120,6,17,17,45));
        Message m9 = Message.builder().text("You said you could handle things").user(Helen).build();
        m9.setCreatedAt(new Date(120,6,17,17,50));
        Message m10 = Message.builder().text("I did what he taught me.").user(Ruth).build();
        m10.setCreatedAt(new Date(120,6,17,18,50));
        m10.setEditedAt(new Date(120,6,17,18,52));
        Message m11 = Message.builder().text("it`s not my fucking fault!").user(Ruth).build();
        m11.setCreatedAt(new Date(120,6,17,18,51));
        Message m12 = Message.builder().text("Can you fix this? Can you fix it?").user(Wendy).build();
        m12.setCreatedAt(new Date(120,6,17,18,53));
        Message m13 = Message.builder().text("Her best is gonna get us all killed.").user(Helen).build();
        m13.setCreatedAt(new Date(120,6,17,18,54));
        m13.setEditedAt(new Date(120,6,17,18,58));
        Message m14 = Message.builder().text("I don`t know how!").user(Ruth).build();
        m14.setCreatedAt(new Date(120,6,17,18,55));
        Message m15 = Message.builder().text("it means that the accounts frozen that cause the feds might think that " +
                "there’s a crime being committed.").user(Ruth).build();
        m15.setCreatedAt(new Date(120,6,17,18,56));
        Message m16 = Message.builder().text("Like by me").user(Ruth).build();
        m16.setCreatedAt(new Date(120,6,18,18,54));
        Message m17 = Message.builder().text("aaaha!").user(Ben).build();
        m17.setCreatedAt(new Date(120,6,19,19,54));
        List<Message> messages = Arrays.asList(m1,m2,m3,m4,m5,m6,m7,m8,m9,m10,m11,m12,m13,m14,m15,m16,m17);
        List<Reaction> reactions = new ArrayList<>();
        for (Message m: messages) {
            for (User u: users) {
                if (((int) (Math.random() * 10)) < 4) reactions.
                        add(Reaction.builder().isLike(true).user(u).message(m).build());
            }
        }
        userRepository.saveAll(users);
        messageRepository.saveAll(messages);
        reactionsRepository.saveAll(reactions);
    }
}
