package com.academy.homework.messages;

import com.academy.homework.messages.dto.MessageDetailDto;
import com.academy.homework.messages.dto.ShortMessageDto;
import com.academy.homework.messages.model.Message;
import com.academy.homework.users.UserRepository;
import com.academy.homework.users.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class MessageService {
    @Autowired
    private MessageRepository messageRepository;

    @Autowired UserRepository userRepository;

    public List<MessageDetailDto> getAllMessages() {
        return messageRepository.getDetailsMessages();
    }

    public ShortMessageDto getMessageById(UUID id) {
        Message message = messageRepository.getMessageById(id).orElseThrow();
        return new ShortMessageDto(message.getId(), message.getText());
    }

    public void editMessage(UUID id,String text) {
        messageRepository.editMessage(id,text);
    }

    public UUID sendMessage(UUID userId, String text) {
        User user = userRepository.findUserById(userId).orElseThrow();
        Message message = Message.builder().text(text).user(user).build();
        messageRepository.save(message);
        return message.getId();
    }

    public void deleteById(UUID id) {
        messageRepository.deleteById(id);
    }
}
