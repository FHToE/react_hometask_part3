package com.academy.homework.messages;

import com.academy.homework.messages.dto.MessageDetailDto;
import com.academy.homework.messages.model.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface MessageRepository extends JpaRepository<Message, UUID> {


    @Query("SELECT new com.academy.homework.messages.dto.MessageDetailDto(m.id, m.text, m.user.name, m.user.avatar," +
            "m.user.id, m.createdAt, (SELECT COALESCE(SUM(CASE WHEN mr.isLike = TRUE THEN 1 ELSE 0 END), 0) " +
            "FROM m.reactions mr WHERE mr.message = m)) FROM Message m " +
            "ORDER BY m.createdAt")
    List<MessageDetailDto> getDetailsMessages();

    Optional<Message> getMessageById(UUID id);

    @Transactional
    @Modifying
    @Query("UPDATE Message m SET m.text = :text WHERE m.id = :id")
    void editMessage(@Param("id") UUID id, @Param("text") String text);

    @Transactional
    @Modifying
    void  deleteById(UUID id);

}
