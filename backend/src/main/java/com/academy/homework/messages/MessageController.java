package com.academy.homework.messages;

import com.academy.homework.messages.dto.MessageDetailDto;
import com.academy.homework.messages.dto.SendMessageRequestDto;
import com.academy.homework.messages.dto.ShortMessageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@CrossOrigin
@RestController
@RequestMapping("/api/messages")
public class MessageController {
    @Autowired
    private MessageService messageService;

    @GetMapping
    public List<MessageDetailDto> getMessages() {
        return messageService.getAllMessages();
    }

    @GetMapping("/{id}")
    public ShortMessageDto getById(@PathVariable UUID id) {
        return messageService.getMessageById(id);
    }

    @PostMapping("/{id}")
    public ShortMessageDto editMessage(@RequestBody ShortMessageDto request) {
        messageService.editMessage(request.getId(), request.getText());
        return messageService.getMessageById(request.getId());
    }

    @PostMapping
    public ShortMessageDto sendMessage(@RequestBody SendMessageRequestDto request) {
        UUID id = messageService.sendMessage(request.getUserId(), request.getText());
        return messageService.getMessageById(id);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void deleteById(@PathVariable UUID id) {
        messageService.deleteById(id);
    }

}
