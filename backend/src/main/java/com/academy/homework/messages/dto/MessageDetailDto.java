package com.academy.homework.messages.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MessageDetailDto {
    private UUID id;
    private String text;
    private String username;
    private String avatar;
    private UUID userId;
    private Date createdAt;
    private long likeCount;
}
