package com.academy.homework.messages.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class SendMessageRequestDto {
    private UUID userId;
    private String text;
}
