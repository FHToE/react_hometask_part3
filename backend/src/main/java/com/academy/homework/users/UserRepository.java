package com.academy.homework.users;

import com.academy.homework.users.dto.UserDto;
import com.academy.homework.users.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {

    @Query("SELECT new com.academy.homework.users.dto.UserDto(u.id, u.name, u.email) from User u " +
            "ORDER BY u.name")
    List<UserDto> getUsers();

    Optional<User> findUserById(UUID id);

    Optional<User> findUserByEmail(String email);

    Optional<User> findUserByName(String name);

    @Transactional
    @Modifying
    void  deleteById(UUID id);
}
