package com.academy.homework.users.dto;

import lombok.Data;


@Data
public class UserAddDto {
    private String name;
    private String email;
    private String password;
}
