package com.academy.homework.users.dto;

import lombok.Builder;
import lombok.Data;

import java.util.UUID;

@Data
@Builder
public class UserDetailDto {
    private UUID id;
    private String name;
    private String email;
    private String password;
}
