package com.academy.homework.users.dto;

import lombok.Data;

@Data
public class LoginDto {
    private String email;
    private String password;
}
