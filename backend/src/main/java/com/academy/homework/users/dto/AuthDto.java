package com.academy.homework.users.dto;

import lombok.Builder;
import lombok.Data;

import java.util.UUID;

@Data
@Builder
public class AuthDto {
    private String role;
    private UUID userId;
    private Boolean isAuthorized;
}
