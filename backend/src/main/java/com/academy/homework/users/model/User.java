package com.academy.homework.users.model;

import com.academy.homework.db.BaseEntity;
import com.academy.homework.messages.model.Message;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper=true)
@Entity
@Table(name = "users")
public class User extends BaseEntity {

    @Column(name = "name", columnDefinition="TEXT")
    private String name;

    @Column(name = "avatar", columnDefinition="TEXT")
    private String avatar;

    @Column(name = "password")
    private String password;

    @Column(name = "email")
    private String email;

    @Column(name = "role")
    private String role;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Message> messages = new ArrayList<>();
}
