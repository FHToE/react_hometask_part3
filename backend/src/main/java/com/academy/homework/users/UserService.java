package com.academy.homework.users;

import com.academy.homework.reactions.model.Reaction;
import com.academy.homework.users.dto.AuthDto;
import com.academy.homework.users.dto.UserDetailDto;
import com.academy.homework.users.dto.UserDto;
import com.academy.homework.users.model.User;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.academy.homework.reactions.ReactionsRepository;

import java.util.List;
import java.util.UUID;

@Service
@Data
public class UserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ReactionsRepository reactionsRepository;

    private UUID currentUserId = UUID.fromString("aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa");

    public List<UserDto> getUsers() {
        return userRepository.getUsers();
    }

    public AuthDto getAuthorization() {
        if (currentUserId.toString().equals("aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa"))
            return AuthDto.builder().isAuthorized(false).build();
        User user = userRepository.findUserById(currentUserId).orElseThrow();
        return AuthDto.builder().isAuthorized(true).role(user.getRole()).userId(user.getId()).build();
    }

    public UserDetailDto fetchById(UUID id) {
        User user = userRepository.findUserById(id).orElseThrow();
        return UserDetailDto.builder().email(user.getEmail())
                .id(user.getId()).name(user.getName()).password(user.getPassword()).build();
    }

    public AuthDto login(String email, String password) {
        var opt = userRepository.findUserByEmail(email);
        if (opt.isEmpty()) return null;
        User user = opt.get();
        if (!user.getPassword().equals(password)) return null;
        currentUserId = user.getId();
        return AuthDto.builder().isAuthorized(true).role(user.getRole()).userId(user.getId()).build();
    }

    public void logout() {
        currentUserId = UUID.fromString("aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa");
    }

    public UserDetailDto updateUser(UUID id, String name, String email, String password) {
        User user = userRepository.findUserById(id).orElseThrow();
        List <UserDto> users = getUsers();
        users.removeIf(u -> u.getId().equals(id));
        if (users.stream().anyMatch(u -> u.getEmail().equalsIgnoreCase(email))) return null;
        user.setName(name);
        user.setEmail(email);
        user.setPassword(password);
        userRepository.save(user);
        user = userRepository.findUserById(id).orElseThrow();
        return UserDetailDto.builder().name(user.getName()).id(user.getId())
                .password(user.getPassword()).email(user.getEmail()).build();
    }

    public UserDetailDto addUser(String name, String email, String password) {
        if (userRepository.findUserByEmail(email).isEmpty() && userRepository.findUserByName(name).isEmpty()) {
            User user = User.builder().avatar("https://forwardsummit.ca/wp-content/uploads/2019/01/avatar-default.png")
                    .email(email).name(name).password(password).build();
            userRepository.save(user);
            return UserDetailDto.builder().email(user.getEmail()).password(user.getPassword())
                    .id(user.getId()).name(user.getName()).build();
        }
        return null;
    }

    public void deleteUser(UUID id) {
        List<Reaction> reactions = reactionsRepository.getReactionsByUser_Id(id);
        reactions.stream().map(r -> r.getId()).forEach(reactionsRepository::deleteById);
        userRepository.deleteById(id);
    }

}
