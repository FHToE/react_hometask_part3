package com.academy.homework.users;

import com.academy.homework.users.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import com.academy.homework.Exception.NotValidDataException;

import java.util.List;
import java.util.UUID;

@CrossOrigin
@RestController
@RequestMapping("/api/users")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("/current")
    public UUID getCurrentUserId(){
        return userService.getCurrentUserId();
    }

    @GetMapping
    public List<UserDto> getUsers() {
        return userService.getUsers();
    }

    @GetMapping("/auth")
    public AuthDto getAuthorization() {
        return userService.getAuthorization();
    }

    @GetMapping("/{id}")
    public UserDetailDto fetchById(@PathVariable UUID id) {
        return userService.fetchById(id);
    }

    @PostMapping("/login")
    public AuthDto login(@RequestBody LoginDto request) {
        AuthDto result = userService.login(request.getEmail(), request.getPassword());
        if (result == null) throw new NotValidDataException();
        return result;
    }

    @PutMapping("/logout")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void logout() {
        userService.logout();
    }

    @PostMapping("/{id}")
    public UserDetailDto updateUser(@RequestBody UserDetailDto request) {
        UserDetailDto result = userService
                .updateUser(request.getId(), request.getName(), request.getEmail(), request.getPassword());
        if (result == null) throw new NotValidDataException();
        return result;
    }

    @PostMapping
    public UserDetailDto addUser(@RequestBody UserAddDto request) {
        UserDetailDto result = userService.addUser(request.getName(), request.getEmail(), request.getPassword());
        if (result == null) throw new NotValidDataException();
        return result;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void deleteById(@PathVariable UUID id) {
        userService.deleteUser(id);
    }
}
