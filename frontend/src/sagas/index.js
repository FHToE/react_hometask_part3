import { all } from  'redux-saga/effects';
import messagesSagas from '../containers/Chat/sagas';
import messageEditSagas from '../containers/MessageEditor/sagas';
import usersEditSagas from '../containers/UserEditor/sagas';
import usersSagas from '../containers/UserList/sagas';
import loginSagas from '../containers/LoginPage/sagas';

export default function* rootSaga() {
    yield all([
        messagesSagas(),
        messageEditSagas(),
        usersEditSagas(),
        usersSagas(),
        loginSagas()
    ])
};