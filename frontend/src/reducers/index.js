import { combineReducers } from 'redux';

import messages from '../containers/Chat/reducer';
import messagePage from '../containers/MessageEditor/reducer';
import users from '../containers/UserList/reducer';
import userPage from '../containers/UserEditor/reducer';
import authorization from '../containers/LoginPage/reducer';

const rootReducer = combineReducers({
    messages,
    messagePage,
    users,
    userPage,
    authorization
});

export default rootReducer;