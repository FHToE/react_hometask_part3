import React, { Component } from 'react';
import { Segment, Form, Button } from 'semantic-ui-react';

class Sender extends Component {
    constructor(props){
        super(props);
        this.state = {
            messageText: ''
        }
       this.handleSend =  this.handleSend.bind(this);
    }

    setText(e) {
        this.setState({messageText: e});
    }

    handleSend() {
        if (this.state.messageText.trim().length === 0) return;
        const message = {
            userId: this.props.userId,
            text: this.state.messageText
        }
        this.props.send(message);
        this.setState({messageText: ''});
    }
    
    render() {
        const data = this.state;
        return (
            <Segment id="sender" style={ { paddingBottom: "50px" ,background: "rgb(181, 200, 207)" }}>
                <Form onSubmit={this.handleSend}>
                    <Form.TextArea
                        name="body"
                        placeholder="Your message..."
                        value={data.messageText}
                        onChange={ev => this.setText(ev.target.value)}
                    />
                    <Button floated="right" color="blue" type="submit">Send</Button>
                </Form>
            </Segment>
        )
    }
}

export default Sender;