import React, { Component } from 'react'
import styles from './styles.module.scss';
import { Popup, Label, Icon, Button } from 'semantic-ui-react';

class RemoveMessage extends Component {
    constructor(props){
        super(props);
        this.handleRemove =  this.handleRemove.bind(this);
        this.handleCancel =  this.handleCancel.bind(this);
    }

    handleRemove() {
        this.props.remove(this.props.messageId);
        document.body.click();
    }

    handleCancel() {
        document.body.click();
    }

    render() {
        const Buttons = () => (
            <div>
                <Button.Group size="mini">
                    <Button onClick={this.handleCancel}>Cancel</Button>
                    <Button.Or />
                    <Button
                        compact
                        color="blue"
                        size="small"
                        onClick={this.handleRemove}
                    >
                        Remove
                    </Button>
                </Button.Group>
            </div>
          );
        return (
            <Popup
              trigger={(
                <Label attached="bottom right" basic size="tiny" as="a" className={styles.toolbarBtn}>
                  <Icon color="pink" name="delete" />
                </Label>
              )}
              on="click"
              position='bottom right'
            >
              {Buttons()}
            </Popup>
          );
    }
}

export default RemoveMessage;