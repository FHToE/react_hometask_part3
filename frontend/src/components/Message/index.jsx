import React, { Component } from 'react';
import { Segment, Image, Label, Grid, Icon, Divider, Form } from 'semantic-ui-react';
import RemoveMessage from '../RemoveMessage'
import styles from './styles.module.scss';

class Message extends Component {
    constructor(props){
        super(props);
        this.state = {
            likedBy:[]
        }
        this.handleLike =  this.handleLike.bind(this);
        this.openEdit =  this.openEdit.bind(this);
    }

    handleLike() {
        const likeList = [...this.state.likedBy];
        const currentUser = this.props.currentUserId;
        if (likeList.includes(currentUser)) {
            const index = likeList.indexOf(currentUser);
            likeList.splice(index, 1);
        } else {
            likeList.push(currentUser);
        }
        this.setState({likedBy: likeList})
    }

    openEdit() {
        this.props.toedit(this.props.id)
    }

    render() {
        const props = this.props;
        const date = new Date(props.createdAt);
        const hours = date.getHours().toString().length === 2? date.getHours().toString() : "0" + date.getHours().toString();
        const minutes = date.getMinutes().toString().length === 2? date.getMinutes().toString() : "0" + date.getMinutes().toString();
        const time = "" + hours + ":" + minutes;
        const isMine = props.currentUserId === props.userId;
        const likeCount = this.state.likedBy.length;

        function getDividerName(inputDate) {
            const msgDate = new Date(inputDate);
            const today = new Date(Date.now());
            today.setHours(0,0,0,0);
            const diff = Math.abs(today - msgDate);
            const diffDays =diff / (1000 * 60 * 60 * 24);
            if (diffDays<=1) {
                return "today";
            }
            if (diffDays > 1 & diffDays <= 2) {
                return "yesterday";
            }
            return "" + msgDate.toDateString();
        }
        const fl = isMine? "right" : "left" ;
        return (
            <Form style={ { display: "inline-block", float: fl }}>
                {props.isFirst ? <Divider section horizontal>{getDividerName(date)}</Divider>:<br/>}
                <Segment raised className={styles.message} size="huge" compact floated={fl}>
                    <Grid columns="2">
                        {isMine? "" : 
                            <Grid.Column textAlign="center" width={3}>
                                <Grid.Row>
                                    <Image floated="left" circular size="small" src={props.avatar} />
                                </Grid.Row>
                                <Grid.Row >                    
                                    <Label color="olive" size="medium" horizontal> {props.user} </Label>
                                </Grid.Row>
                            </Grid.Column>
                        }
                            <Grid.Column style={{ wordWrap: 'break-word' }} textAlign="center" width={isMine? 14 : 11}>
                                <Segment textAlign="left">
                                    {props.text}
                                </Segment>
                            </Grid.Column>
                                { isMine? 
                                <div>
                                  <Label
                                    attached="bottom right"
                                    basic size="tiny"
                                    as="a"
                                    className={styles.toolbarBtnHidden}
                                    onClick={this.openEdit}
                                  >
                                      <Icon color="grey" name="cog">
                                        {'\u2007\u2007\u2007'}
                                      </Icon>
                                  </Label>
                                  <RemoveMessage messageId={props.id} remove={props.remove} /> 
                                </div>  
                                    :
                                    <Label basic attached="bottom right" size="large" as="a" className={styles.toolbarBtn} onClick={this.handleLike}>
                                        <Icon name={likeCount === 0 ? "thumbs up outline" : "thumbs up"} />
                                        {likeCount === 0 ? '\u2007' : likeCount}
                                    </Label>
                                }
                        </Grid>
                        <Label size="small" attached="top right" horizontal> {time} </Label>
                </Segment>
                
                </Form>
        );
    };
}

export default Message;