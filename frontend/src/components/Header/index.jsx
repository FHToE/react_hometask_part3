import { Grid } from 'semantic-ui-react';
import styles from './styles.module.scss';
import React from 'react';

const Header = ({users, mesCount, lastMsgDate, handleFilter}) =>
    <Grid className={styles.header} centered container columns="12">
        <Grid.Row centered verticalAlign="middle" color="blue" >
            <Grid.Column className="chat" width="2" textAlign="center" floated="left">
                My chat:)
            </Grid.Column>
            <Grid.Column className={styles.users} width="5" textAlign="left" floated="left">
                {users.length} participants
            </Grid.Column>
            <Grid.Column width="2" textAlign="left" floated="left">
                {mesCount} messages
            </Grid.Column>
            <Grid.Column className="lastMsg" width="4" textAlign="center" floated="right">
                Last message at {lastMsgDate}
            </Grid.Column>
        </Grid.Row>
    </Grid>

export default Header;