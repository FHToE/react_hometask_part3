import { Segment, Button } from 'semantic-ui-react';
import React, { useState } from 'react';
import PropTypes from 'prop-types';

const UserSegment = ({ user, deleteUser, toEdit }) => {
    
    const handleEdit = () => {
        toEdit(user.id);
    }

    const [isLoading, setIsLoading] = useState(false);
    const handleDelete = () => {
        setIsLoading(true);
        deleteUser(user.id);
        setIsLoading(false);
    }
    return (
        <Segment.Group horizontal>
            <Segment style={ { width: "350px" }}>{user.name}</Segment>
            <Segment style={ { width: "350px" }}>{user.email}</Segment>
            <Segment style={ { width: "200px" }}>
                <Button color="blue" onClick={() => handleEdit()}>Edit</Button>
                <Button color="grey" loading={isLoading} onClick={() => handleDelete()}>Delete</Button>
            </Segment>
        </Segment.Group>
    );
}

UserSegment.propTypes = {
    deleteUser: PropTypes.func.isRequired
}


export default UserSegment;