import { Segment, Header, Icon, Button } from 'semantic-ui-react';
import React from 'react';
import PropTypes from 'prop-types';
import styles from './styles.module.scss';

const AdminPanel = ({ toChat, toUsers }) => {
    
    return (
        <Segment className={styles.panel}>
             <Header icon >
                <Icon name='adn' />
                    Admin Panel
            </Header>
            <Segment.Inline>
            <Button color="teal" onClick={() => toChat()} >Chat</Button>
            <Button color="teal" onClick={() => toUsers()} >Users</Button>
            </Segment.Inline>
        </Segment>
    )
}

AdminPanel.propTypes = {
    toChat: PropTypes.func.isRequired,
    toUsers: PropTypes.func.isRequired
}


export default AdminPanel;