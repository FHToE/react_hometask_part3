import { call, put, takeEvery, all } from 'redux-saga/effects';
import { LOAD_ALL_MESSAGES, SEND_MESSAGE, EDIT_MESSAGE, DELETE_MESSAGE, MESSAGE_REACTION, LOAD_ALL_MESSAGES_SUCCESS } from './actionTypes';
import * as service from './service';
import * as userService from '../UserList/service';

export function* fetchMessages() {
    try {
        const messages = yield call(service.fetchMessages);
        const currentUserId = yield call(userService.getCurrentUserId);
        yield put({type: LOAD_ALL_MESSAGES_SUCCESS, payload: { messages, currentUserId }});
    } catch (error) {
        console.log("error: " + error.message);
    }
}

function* watchFetchMessages() {
    yield takeEvery(LOAD_ALL_MESSAGES, fetchMessages)
}

export function* sendMessage(action) {
    const newMessage = { ...action.payload.message };
    try {
        yield call(service.sendMessage, newMessage);
        yield put({ type: LOAD_ALL_MESSAGES });
    } catch (error) {
        console.log("error: " + error.message);
    }
}

function* watchSendMessage() {
    yield takeEvery(SEND_MESSAGE, sendMessage);
}

export function* editMessage(action) {
    const updatedMessage = {...action.payload.message};
    try {
        yield call(service.editMessage, updatedMessage);
        yield put({ type: LOAD_ALL_MESSAGES });
    } catch (error) {
        console.log("error: " + error.message);
    }
}

function* watchEditMessage() {
    yield takeEvery(EDIT_MESSAGE, editMessage);
}

export function* deleteMessage(action) {
    const id = action.payload.id;
    try {
        yield call(service.deleteMessage, id);
        yield put({ type: LOAD_ALL_MESSAGES });
    } catch (error) {
        console.log("error: " + error.message);
    }
}

function* watchDeleteMessage() {
    yield takeEvery(DELETE_MESSAGE, deleteMessage);
}

export function* likeMessage(action) {
    const reaction = {...action.payload.reaction};
    try {
        yield call(service.sendReaction, reaction);
        yield put({ type: LOAD_ALL_MESSAGES });
    } catch (error) {
        console.log("error: " + error.message);
    }
}

function* watchLikeMessage() {
    yield takeEvery(MESSAGE_REACTION, likeMessage);
}

export default function* messagesSagas() {
    yield all([
        watchDeleteMessage(),
        watchEditMessage(),
        watchFetchMessages(),
        watchSendMessage(),
        watchLikeMessage()
    ])
};