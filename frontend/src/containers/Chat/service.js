import callWebApi from '../../helpers/webApiHelper';

export const fetchMessages = async () => {
    const response = await callWebApi({
        endpoint: '/api/messages',
        type: 'GET'
      });
      return response.json();
}

export const fetchMessageById = async id => {
    const response = await callWebApi({
        endpoint: `/api/messages/${id}`,
        type: 'GET'
      });
      return response.json();
}

export const editMessage = async request => {
    const response = await callWebApi({
      endpoint: `/api/messages/${request.id}`,
      type: 'POST',
      request
    });
    return response.json();
};

export const sendMessage = async request => {
    const response = await callWebApi({
      endpoint: '/api/messages',
      type: 'POST',
      request
    });
    return response.json();
};

export const sendReaction = async request => {
  const response = await callWebApi({
    endpoint: '/api/reactions',
    type: 'POST',
    request
  });
  return response.json();
};

export const deleteMessage = async id => callWebApi({ endpoint: `/api/messages/${id}`, type: 'DELETE' });

export const getNewId = () => ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
    (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16));

