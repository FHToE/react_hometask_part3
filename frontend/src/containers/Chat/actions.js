import { LOAD_ALL_MESSAGES, SEND_MESSAGE, EDIT_MESSAGE, DELETE_MESSAGE, MESSAGE_REACTION } from './actionTypes';

export const fetchMessages = () => ({
    type: LOAD_ALL_MESSAGES
});

export const sendMessage = message => ({
    type: SEND_MESSAGE,
    payload: {
        message
    }
});

export const editMessage = message => ({
    type: EDIT_MESSAGE,
    payload: {
        message
    }
});

export const deleteMessage = id => ({
    type: DELETE_MESSAGE,
    payload: {
        id
    }
});

export const likeMessage = reaction => ({
    type: MESSAGE_REACTION,
    payload: {
        reaction
    }
});
