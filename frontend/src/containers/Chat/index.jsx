import React, { Component } from 'react';
import { Loader, Segment } from 'semantic-ui-react';
import Message from '../../components/Message';
import Header from '../../components/Header';
import Sender from '../../components/Sender';
import * as actions from './actions';
import { connect } from 'react-redux';
import styles from './styles.module.scss';
import AdminPanel from '../../components/AdminPanel';

class Chat extends Component {
    constructor(props){
        super(props);
        this.handleSend =  this.handleSend.bind(this);
        this.handleEdit =  this.handleEdit.bind(this);
        this.handleRemove =  this.handleRemove.bind(this);
    }
    
    handleSend(msg) {
        this.props.sendMessage({ userId: this.props.currentUserId, text: msg.text });
    }

    handleEdit(msg) {
        this.props.editMessage({id: msg.id, text: msg.text});
    }

    handleRemove(id) {
        this.props.deleteMessage(id);
    }

    componentDidMount() {
        this.props.fetchMessages();
    }

    componentDidUpdate() {
        if (!this.props.isAuthorized) {
            this.props.history.push("/") 
        } else {
            const scrolling = document.getElementById("scroller");
            scrolling.scrollTop = scrolling.scrollHeight;
        }
    }
    

    render() {
        const data = this.props;
        var dateContainer = "";
        function isFirst(date) {
            const msgDate = new Date(date);
            const toContainer = "" + msgDate.getMonth() + msgDate.getDate();
            if (toContainer !== dateContainer) {
                dateContainer = toContainer;
                return true; 
            }
            return false;
        }

        function getUsers() {
            return [...new Set(data.messages.map(m=>m.userId))];
        }

        function getLastMsgDate() {
            const sorted = data.messages.sort((m1, m2) => (new Date(m1.createdAt)-new Date(m2.createdAt)));
            const mcount = sorted.length;
            const date = new Date(sorted[mcount-1].createdAt);
            const hours = date.getHours().toString().length === 2? date.getHours().toString() : "0" + date.getHours().toString();
            const minutes = date.getMinutes().toString().length === 2? date.getMinutes().toString() : "0" + date.getMinutes().toString();
            return "" + hours + ":" + minutes;
        }
        
        const handleToChat = () => this.props.history.push("/chat");
        const handleToUserList = () => this.props.history.push("/users");
        const handleToEditMessage = (id) => this.props.history.push(`/message/${id}`);

        return(
        <div>
            <div><img alt="bsa" src={require('../../resources/academy.png')}  className={styles.logo}></img></div>
            {data.role === "admin" ? <AdminPanel toChat={handleToChat} toUsers={handleToUserList}/> : ""}
            <div className={styles.content}>
                
                {!data.isLoaded? <Loader active inline="centered" /> : 
                <Header
                    users = {getUsers()}
                    mesCount = {data.messages.length}
                    lastMsgDate = {getLastMsgDate()}
                />
                }
                {!data.isLoaded? "" : 
                <Segment id="scroller" raised style={ {overflow: "auto", height: "27em", background: "rgb(181, 200, 207)" }}>
                    {data.messages.map(mes=>
                            <Message 
                                isFirst = {isFirst(mes.createdAt)}
                                key =  {mes.id}
                                id = {mes.id}
                                user = {mes.username}
                                avatar = {mes.avatar}
                                createdAt = {mes.createdAt}
                                text = {mes.text}
                                userId = {mes.userId}
                                currentUserId = {data.currentUserId}
                                remove = {this.handleRemove}
                                toedit = {handleToEditMessage}
                            />)}
                </Segment >
                }
                {!data.isLoaded? "" : 
                <Sender userId={data.currentUserId} send={this.handleSend}/>
            }
            </div>
        </div>
        ); 
    }
};

const mapStateToProps = state => {
    return {
        isAuthorized: state.authorization.isAuthorized,
        role: state.authorization.role,
        messages: state.messages.messages,
        isLoaded: state.messages.isLoaded,
        currentUserId: state.messages.currentUserId
    }
}

const mapDispatchToProps = {...actions};

export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(Chat);
