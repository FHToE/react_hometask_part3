
import { LOAD_ALL_MESSAGES_SUCCESS } from './actionTypes';

export default function (state = {}, action) {
    switch (action.type) {
        case LOAD_ALL_MESSAGES_SUCCESS: {
            return {
                ...state,
                messages: action.payload.messages,
                isLoaded: true,
                currentUserId: action.payload.currentUserId
            };
        }
        default:
            return state;        
    }
}
