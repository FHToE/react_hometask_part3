import { LOGIN_SUCCESS, LOGOUT_SUCCESS, REQUEST_FAILED } from './actionTypes';

export default function (state = {}, action) {
    switch (action.type) {
        case LOGIN_SUCCESS: {
            const role = action.payload.role;
            const currentUserId = action.payload.userId;
            const isAuthorized = action.payload.isAuthorized;
            return {
                ...state,
                role,
                currentUserId,
                isAuthorized
            };
        };
        case LOGOUT_SUCCESS: {
            return {
                ...state,
                role: undefined,
                currentUserId: undefined,
                isAuthorized: false,
                badRequest: false
            };
        };
        case REQUEST_FAILED: {
            return {
                ...state,
                badRequest: true
            };
        };
        default:
            return state;
    }
};
