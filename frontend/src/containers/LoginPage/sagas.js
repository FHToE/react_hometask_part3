import { call, put, takeEvery, all } from 'redux-saga/effects';
import { LOGIN, LOGIN_SUCCESS, LOGOUT, LOGOUT_SUCCESS, GET_AUTHORIZATION, REQUEST_FAILED } from './actionTypes';
import * as service from '../../containers/UserList/service';

export function* login(action) {
    try {
        const result = yield call(service.login, action.payload.user);
        yield put({ type: LOGIN_SUCCESS, payload: {role: result.role, userId: result.userId, isAuthorized: result.isAuthorized }}); 
    } catch (error) {
        yield put({ type: REQUEST_FAILED, payload: { error } });
    }
}
        

function* watchLogin() {
    yield takeEvery(LOGIN, login);
}

export function* getAuthorization() {
    const result = yield call(service.getAuthorization);
    yield put({ type: LOGIN_SUCCESS, payload: {role: result.role, userId: result.userId, isAuthorized: result.isAuthorized }})
}

function* watchGetAuthorization() {
    yield takeEvery(GET_AUTHORIZATION, getAuthorization);
}

export function* logout() {
    try {
        yield call(service.logout);
        yield put({ type: LOGOUT_SUCCESS, payload: {}})
    } catch (error) {
        console.log("error: " + error.message);
    }
}

function* watchLogout() {
    yield takeEvery(LOGOUT, logout);
}

export default function* loginSagas() {
    yield all([
        watchLogin(),
        watchLogout(),
        watchGetAuthorization()
    ])
};
