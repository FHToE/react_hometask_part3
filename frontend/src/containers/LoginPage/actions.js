import { LOGIN, LOGOUT, GET_AUTHORIZATION } from './actionTypes';

export const login = user => ({
    type: LOGIN,
    payload: {
        user
    }
});

export const logout = () => ({
    type: LOGOUT,
    payload: {}
});

export const getAuthorization = () => ({
    type: GET_AUTHORIZATION,
    payload: {}
});
