import { login, getAuthorization } from './actions';
import React, { useEffect } from 'react'; 
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { Header, Grid, Form } from 'semantic-ui-react';
import LoginForm from   '../../components/LoginForm';
import styles from './styles.module.scss';

const LoginPage = ({ login: signIn, role, isAuthorized, getAuthorization, badRequest }) => {

    useEffect(() => {
        if (!isAuthorized) {
            getAuthorization();
        }
      });

    const loginElement = () => (
        <Form className={styles.login}>
            <Grid textAlign="center" verticalAlign="middle" >
                <Grid.Column style={{ maxWidth: 450 }}>
                    <Header as="h2" color="blue" textAlign="center">
                        Login to your account
                    </Header>
                    <LoginForm login={signIn} err={badRequest}/>
                </Grid.Column>
            </Grid>
        </Form>    
    );

    switch (role) {
        case "user": {
            return <Redirect to={{ pathname: '/chat' }} />;
        }
        case "admin": {
            return <Redirect to={{ pathname: '/users' }} />;
        }
        default: {
            return loginElement();
        }
    }
}

LoginPage.propTypes = {
    login: PropTypes.func.isRequired,
    getAuthorization: PropTypes.func.isRequired
};

const actions = { login, getAuthorization };

const mapStateToProps = state => {
    return {
        role: state.authorization.role,
        isAuthorized: state.authorization.isAuthorized,
        badRequest: state.authorization.badRequest
    }
}

LoginPage.defaultProps = {
    role: undefined,
    isAuthorized: false
  };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(LoginPage);

