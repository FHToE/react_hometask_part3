import { LOAD_ALL_USERS, ADD_USER, EDIT_USER, DELETE_USER } from './actionTypes';

export const addUser = user => ({
    type: ADD_USER,
    payload: {
        user
    }
});

export const fetchUsers = () => ({
    type: LOAD_ALL_USERS
});

export const updateUser = user => ({
    type: EDIT_USER,
    payload: {
        user
    }
});

export const deleteUser = id => ({
    type: DELETE_USER,
    payload: {
        id
    }
});