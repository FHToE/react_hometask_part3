export const LOAD_ALL_USERS = 'LOAD_ALL_USERS';
export const ADD_USER = 'ADD_USER';
export const EDIT_USER = 'EDIT_USER';
export const DELETE_USER = 'DELETE_USER';
export const LOAD_ALL_USERS_SUCCESS = 'LOAD_ALL_USERS_SUCCESS';
