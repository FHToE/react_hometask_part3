import { call, put, takeEvery, all } from 'redux-saga/effects';
import { LOAD_ALL_USERS, ADD_USER, EDIT_USER, DELETE_USER, LOAD_ALL_USERS_SUCCESS } from './actionTypes';
import { UPDATE_FAILED } from '../UserEditor/actionTypes';
import * as service from './service';

export function* fetchUsers() {
    try {
        const users = yield call(service.fetchUsers);
        yield put({type: LOAD_ALL_USERS_SUCCESS, payload: { users }});
    } catch (error) {
        console.log("error: " + error.message);
    }
}

function* watchFetchUsers() {
    yield takeEvery(LOAD_ALL_USERS, fetchUsers)
}

export function* addUser(action) {
    const newUser = { ...action.payload.user };
    yield call(service.addUser, newUser);
    yield put({ type: LOAD_ALL_USERS });
}

function* watchAddUser() {
    yield takeEvery(ADD_USER, addUser);
}

export function* updateUser(action) {
    const updatedUser = {...action.payload.user};
    try {
        yield call(service.updateUser, updatedUser);
        yield put({ type: LOAD_ALL_USERS });
    } catch (error) {
        console.log("error: " + error.message);
        yield put({ type: UPDATE_FAILED, payload: { error } });
    }
}

function* watchUpdateUser() {
    yield takeEvery(EDIT_USER, updateUser);
}

export function* deleteUser(action) {
    const id = action.payload.id;
    try {
        yield call(service.deleteUser, id);
        yield put({ type: LOAD_ALL_USERS });
    } catch (error) {
        console.log("error: " + error.message);
    }
}

function* watchDeleteUser() {
    yield takeEvery(DELETE_USER, deleteUser);
}

export default function* usersSagas() {
    yield all([
        watchFetchUsers(),
        watchAddUser(),
        watchUpdateUser(),
        watchDeleteUser()
    ])
};
