import UserSegment from '../../components/UserSegment';
import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Header, Loader, Button } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';
import { fetchUsers, deleteUser } from './actions';
import styles from './styles.module.scss';
import AdminPanel from '../../components/AdminPanel';

const UserList = ({ users, history, isLoaded, fetchUsers, isAuthorized, role, deleteUser: del }) => {
    useEffect(() => {
        if (!isLoaded) {
            fetchUsers();
        }
      });
    const handleAdd = () => {
        history.push("/user");
    };

    const handleToChat = () => {
        history.push("/chat");
    };

    const handleToUserList = () => {
        history.push("/users");
    }

    const handleToEdit = (id) => {
        history.push(`/user/${id}`);
    }

    return (
        <div>
            {role === "admin" ? <AdminPanel toChat={handleToChat} toUsers={handleToUserList}/> : <Redirect to={{ pathname: '/chat' }} />}
            {isAuthorized ? 
            <div className={styles.users}>
            {isLoaded? 
                <div>
                <Header >UserList</Header>
                {users.map(user => (
                    <UserSegment
                        key = {user.id}
                        user = {user}
                        deleteUser = {del}
                        toEdit = {handleToEdit}
                    />
                    ))}
                <Button color="blue" onClick={() => handleAdd()}>Add User</Button>
                </div>
            : <Loader active inline="centered" />}
            </div> :
            <Redirect to="/" />
        }
        </div>
        
    );
}

UserList.propTypes = {
    history: PropTypes.objectOf(PropTypes.any)
}

const mapStateToProps = state => {
    return {
        users: state.users.users,
        isLoaded: state.users.isLoaded,
        isAuthorized: state.authorization.isAuthorized,
        role: state.authorization.role
    }
}

UserList.defaultProps = {
    users: [],
    isLoaded: false,
    history: undefined,
    isAuthorized: false,
    role: ""
  };

const actions = { fetchUsers, deleteUser };

const mapDispatchToProps = {...actions};

export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(UserList);