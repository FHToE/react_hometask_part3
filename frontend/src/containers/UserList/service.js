import callWebApi from '../../helpers/webApiHelper';

export const fetchUsers = async () => {
    const response = await callWebApi({
        endpoint: '/api/users',
        type: 'GET'
      });
      return response.json();
}

export const getAuthorization = async () => {
  const response = await callWebApi({
      endpoint: '/api/users/auth',
      type: 'GET'
    });
    return response.json();
}

export const fetchUserById = async id => {
    const response = await callWebApi({
        endpoint: `/api/users/${id}`,
        type: 'GET'
      });
      return response.json();
}

export const login = async request => {
  const response = await callWebApi({
      endpoint: '/api/users/login',
      type: 'POST',
      request
  });
  return response.json();
}

export const logout = async () => await callWebApi({ endpoint: '/api/users/logout', type: 'PUT' });

export const getCurrentUserId = async () => {
  const response = await callWebApi({
    endpoint: '/api/users/current',
    type: 'GET'
  });
  return response.json();
}

export const updateUser = async request => {
  const response = await callWebApi({
    endpoint: `/api/users/${request.id}`,
    type: 'POST',
    request
  });
  return response.json();
};

export const addUser = async request => {
    const response = await callWebApi({
      endpoint: '/api/users',
      type: 'POST',
      request
    });
    return response.json();
};

export const deleteUser = async id => callWebApi({ endpoint: `/api/users/${id}`, type: 'DELETE' });