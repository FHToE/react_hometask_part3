import { LOAD_ALL_USERS_SUCCESS } from './actionTypes';

export default function (state = {}, action) {
    switch (action.type) {
        case LOAD_ALL_USERS_SUCCESS: {
            return {
                ...state,
                users: action.payload.users,
                isLoaded: true
            };
        };
        default:
            return state;
    }
};
