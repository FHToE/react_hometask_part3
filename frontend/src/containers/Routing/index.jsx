import { Route, Switch } from 'react-router-dom';
import React from 'react';
import Chat from '../Chat';
import MessageEditor from '../MessageEditor';
import LoginPage from '../LoginPage';
import UserList from '../UserList';
import UserEditor from '../UserEditor';
import { Button, Icon } from 'semantic-ui-react';
import { logout } from '../LoginPage/actions';
import { connect } from 'react-redux';
import styles from './styles.module.scss';
import PropTypes from 'prop-types';

const Routing = ({ currentUserId, logout }) => {
    return (
    <div>
        {currentUserId? 
        <Button icon className={styles.logout} labelPosition="right" onClick={() => logout()}>
            Logout
            <Icon name="log out" />
        </Button> : ""}
        <main className="fill">
            <Switch>
                <Route exact path="/" component={LoginPage} />
                <Route exact path="/chat" component={Chat} />
                <Route exact path="/users" component={UserList} />
                <Route path="/message/:id" component={MessageEditor} />
                <Route path="/user/:id" component={UserEditor} />
                <Route exact path="/user" component={UserEditor} />
            </Switch>
        </main>
    </div>);
};
   


Routing.defaultProps = {
    currentUserId: undefined,
    role: undefined
};

Routing.propTypes = {
    logout: PropTypes.func.isRequired
};

const mapStateToProps = state => {
    return {
    currentUserId: state.authorization.currentUserId
    }
};

const actions = { logout };

const mapDispatchToProps = {...actions};

export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(Routing);

