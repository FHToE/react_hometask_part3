import { FETCH_USER_SUCCESS, UPDATE_FAILED } from './actionTypes';

const initialState = {
    isLoaded: true,
    isSuccess: false,
    userData: {
        id: "",
        name: "",
        email: "",
        password: ""
    }
};

export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_USER_SUCCESS: {
            const userData = {
                id: action.payload.user.id,
                name: action.payload.user.name,
                email: action.payload.user.email,
                password: action.payload.user.password
            };
            return {
                ...state,
                isSuccess: true,
                isLoaded: true,
                userData
            }
        };
        case UPDATE_FAILED: {
            return {
                ...state,
                isSuccess: false
            }
        }
        default:
            return state;
    }
}