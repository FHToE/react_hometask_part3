import { call, put, takeEvery, all } from 'redux-saga/effects';
import { FETCH_USER, FETCH_USER_SUCCESS } from './actionTypes';
import * as service from '../../containers/UserList/service';

export function* fetchUser(action) {
    try {
        const id = action.payload.id;
        const user = yield call(service.fetchUserById, id);
        yield put({ type: FETCH_USER_SUCCESS, payload: { user } })
    } catch (error) {
        console.log("error: " + error.message);
    }
}

function* watchFetchUser() {
    yield takeEvery(FETCH_USER, fetchUser);
}

export default function* usersEditSagas() {
    yield all([
        watchFetchUser()
    ])
};
