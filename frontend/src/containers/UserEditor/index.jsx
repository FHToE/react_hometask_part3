import React, { useState, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import validator from 'validator';
import { Header, Button, Loader, Input, Label, Icon, Form } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchUser } from './actions';
import { updateUser, addUser } from '../UserList/actions';
import AdminPanel from '../../components/AdminPanel';
import styles from './styles.module.scss';

const UserEditor = ({ userData, fetchUser, updateUser, addUser, isLoaded, match, isAuthorized, history, role, isSuccess }) => {
    const [newName, setNewName] = useState("");
    const [loaded, setLoaded] = useState(isLoaded);
    const [newEmail, setNewEmail] = useState("");
    const [newPassword, setNewPassword] = useState("");
    useEffect(() => {
        if (match.params.id) {
            setLoaded(false);
            fetchUser(match.params.id);
        }
        setNewName(userData.name);
        setNewEmail(userData.email);
        setNewPassword(userData.password);
        setLoaded(true);
      }, [match.params.id, fetchUser, userData.name, userData.email, userData.password]);

    const [isNameValid, setIsNameValid] = useState(true);
    const [isEmailValid, setIsEmailValid] = useState(true);
    const [isPasswordVisible, setIsPasswordVisible] = useState(false);
    const [isPasswordValid, setIsPasswordValid] = useState(true);
    const isChanged = (userData.name!==newName || userData.email!==newEmail || userData.password!==newPassword);
    const [isLoading, setIsLoading] = useState(false);
    const [isDone, setIsDone] = useState(false);

    const emailChanged = data => {
        setNewEmail(data);
        setIsEmailValid(true);
      };

    const nameChanged = data => {
        setNewName(data);
        setIsNameValid(true);
    };

    const passwordChanged = data => {
        setNewPassword(data);
        setIsPasswordValid(true);
    };

    const handleSave = () => {
        setIsLoading(true);
        if (match.params.id) {
            updateUser({id: userData.id, name: newName, email: newEmail, password: newPassword });
        } else {
            try {
            addUser({ name: newName, email: newEmail, password: newPassword });
            } catch (error) {
            }
        }
        setIsDone(true);
        setIsLoading(false);
    }

    const handleCancel = () => {
        setNewName("");
        setLoaded(false);
        setNewEmail("");
        setNewPassword("");
        history.push("/users");
    };

    const handleToChat = () => {
        history.push("/chat");
    };

    const handleToUserList = () => {
        history.push("/users");
    }

    return (
        <div>
            {role === "admin" ? <AdminPanel toChat={handleToChat} toUsers={handleToUserList}/> : <Redirect to={{ pathname: '/chat' }} />}
            {isAuthorized ? 
            <div className={styles.content}>
            {!loaded ? <Loader active inline="centered" /> :
                <div>
                <Header as="h3" dividing textAlign="center">
                    User
                </Header>
                <br/>
                <div>
                    <Input
                        fluid
                        icon="text cursor"
                        iconPosition="left"
                        type="text"
                        error={!isNameValid}
                        value={newName}
                        onChange={ev => nameChanged(ev.target.value)}
                        onBlur={() => setIsNameValid(Boolean(newName))}
                    />
                </div>
                <br/>
                <div>
                    <Input
                        fluid
                        icon="at"
                        iconPosition="left"
                        type="email"
                        error={!isEmailValid}
                        value={newEmail}
                        onChange={ev => emailChanged(ev.target.value)}
                        onBlur={() => setIsEmailValid(validator.isEmail(newEmail))}
                    />
                </div>
                <br/>
                <div>
                    <Form>
                        <Input
                            fluid
                            icon="lock"
                            iconPosition="left"
                            placeholder="Password"
                            value={newPassword}
                            type={isPasswordVisible? "text" : "password"}
                            error={!isPasswordValid}
                            onChange={ev => passwordChanged(ev.target.value)}
                            onBlur={() => setIsPasswordValid(Boolean(newPassword))}
                        />
                        <Label
                            basic
                            attached="top right"
                            size="tiny"
                            as="a"
                            className={styles.toolbarBtn}
                            onClick={() => setIsPasswordVisible(!isPasswordVisible)}
                        >
                            <Icon color="grey" size="large" name="eye"/>
                        </Label>
                    </Form>
                </div>
                <br/>
                {isSuccess && isDone ? 
                    <Button
                    size="small"
                    color="blue"
                    onClick={() => handleCancel()}
                    >
                        Back to Users
                    </Button> : 
                    <div>
                        <Button
                            disabled={!isChanged}
                            size="small"
                            color="blue"
                            onClick={() => handleSave()}
                            loading={isLoading}
                        >
                            Save
                        </Button>
                        <Button size="small" color="grey" onClick={() => handleCancel()}>Cancel</Button>
                    </div> 
                } 
                
                <div>
                <br/>
                {!isSuccess && isDone ? <Label color="pink" size="large">
                    Ooops, something went wrong! Probably user with such data already exists.
                </Label> : "" } 
                {isSuccess && isDone ? <Label color="green" size="large">
                    User data upated successfull
                </Label> : "" } 
                </div>
            </div>
            }
            </div> : <Redirect to="/" />
        }
        </div>
        
    );
}

UserEditor.propTypes = {
    userData: PropTypes.objectOf(PropTypes.any).isRequired,
    history: PropTypes.objectOf(PropTypes.any),
    match: PropTypes.objectOf(PropTypes.any),
    isLoaded: PropTypes.bool.isRequired,
    fetchUser: PropTypes.func.isRequired,
    addUser: PropTypes.func.isRequired,
    updateUser: PropTypes.func.isRequired
}


const mapStateToProps = state => {
    return {
        userData: state.userPage.userData,
        isLoaded: state.userPage.isLoaded,
        isSuccess: state.userPage.isSuccess,
        isAuthorized: state.authorization.isAuthorized,
        role: state.authorization.role
    }
}

UserEditor.defaultProps = {
    isLoaded: true,
    isAuthorized: false,
    userData: undefined,
    match: undefined,
    history: undefined,
    role: ""
};

const actions = {
    fetchUser,
    updateUser,
    addUser
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(UserEditor);
