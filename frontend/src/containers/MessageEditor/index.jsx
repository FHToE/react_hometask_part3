import React, { useState, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Header, Icon, Form, Segment, Button, Loader } from 'semantic-ui-react';
import { editMessage } from '../Chat/actions';
import { fetchMessage } from './actions';
import AdminPanel from '../../components/AdminPanel';

const MessageEditor = ({ message, isLoaded, editMessage, fetchMessage, match, isAuthorized, role, history }) => {
    const [body, setBody] = useState(message);
    const [loaded, setLoaded] = useState(isLoaded);
    useEffect(() => {
        if (match.params.id) {
            setLoaded(false);
            fetchMessage(match.params.id);
        }
        if (body==="") {
            setBody(message.text);
        }
        setLoaded(true);
      }, [match.params.id, body, fetchMessage, message.text]);
    
    const [isLoading, setIsLoading] = useState(false);

    const handleToChat = () => {
        history.push("/chat");
    };

    const handleToUserList = () => {
        history.push("/users");
    }

    const handleSave= () => {
        if (body === "" || body === message.text) return;
        setIsLoading(true);
        editMessage({ id: message.id, text: body});
        setIsLoading(false);
        handleToChat();
    }

    return (
        <div>
            {role === "admin" ? <AdminPanel toChat={handleToChat} toUsers={handleToUserList}/> : ""}
            {isAuthorized? 
            <div>
                {loaded?
                <div>
                    <Header dividing as='h2'>
                        <Icon name='settings' />
                        <Header.Content>
                            Edit your message...
                        </Header.Content>
                    </Header>
                    <Segment>
                        <Form onSubmit={() => handleSave()}>
                            <Form.TextArea
                                name="body"
                                value={body}
                                onChange={ev => setBody(ev.target.value)}
                            />
                            <Button floated="right" color="grey" onClick={handleToChat()}>Cancel</Button>
                            <Button floated="right" loading={isLoading} disabled={message.text===body} color="blue" type="submit">Save</Button>
                        </Form>
                    </Segment>
                </div> : <Loader active inline="centered" /> }
            </div> : <Redirect to="/" />}
        </div>
           
    );
};

MessageEditor.propTypes = {
    message: PropTypes.objectOf(PropTypes.any).isRequired,
    match: PropTypes.objectOf(PropTypes.any),
    isLoaded: PropTypes.bool.isRequired,
    editMessage: PropTypes.func.isRequired,
    fetchMessage: PropTypes.func.isRequired,
    history: PropTypes.objectOf(PropTypes.any)
}


const mapStateToProps = state => {
    return {
        message: state.messagePage.message,
        isLoaded: state.messagePage.isLoaded,
        isAuthorized: state.authorization.isAuthorized,
        role: state.authorization.role
    }
}

MessageEditor.defaultProps = {
    isAuthorized: false,
    isLoaded: false,
    match: undefined,
    role: ""
};

const actions = {
    editMessage,
    fetchMessage
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(MessageEditor);