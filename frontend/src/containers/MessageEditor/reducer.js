import { FETCH_MESSAGE_SUCCESS } from './actionTypes';

const initialState = {
    isLoaded: true,
    message: {
        id: "",
        text: ""
    }
};



export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_MESSAGE_SUCCESS: {
            const message = action.payload.message;
            return {
                ...state,
                message,
                isLoaded: true
            };
        };
        default:
            return state;
    }
};
