import React from 'react';
import { render } from 'react-dom';
import Routing from './containers/Routing';
import 'semantic-ui-css/semantic.min.css';
import './styles/common.scss';
import { BrowserRouter as Router } from 'react-router-dom';
import configureStore from './store/configureStore';
import { Provider } from 'react-redux';


const store = configureStore();
const target = document.getElementById('root');
render(
    <Provider store={store}>
        <Router>
            <Routing />
        </Router>
    </Provider>
    , target);
